﻿using UnityEngine;
using System.Collections;

public class Player2 : MonoBehaviour {

    private Rigidbody2D Rigidbody;
    public float m_Rotation_Angle;
    public float m_Movement_Speed;

    Vector3 newPosition;
    // Use this for initialization
    void Start()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        newPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Rotation();
    }

    void Rotation()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            transform.Rotate(transform.forward * m_Rotation_Angle);
            newPosition += (transform.up * m_Movement_Speed * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            transform.Rotate(transform.forward * -m_Rotation_Angle);
            newPosition += (transform.up * m_Movement_Speed * Time.deltaTime);
        }
        transform.position = Vector3.Lerp(transform.position, newPosition, 0.01f);
    }

}