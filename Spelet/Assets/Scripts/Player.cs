﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    private Rigidbody2D RBody;
    public float m_Rotation_Angle;
    public float m_Movement_Speed;
    Vector3 newPosition;

    // Use this for initialization
    void Start()
    {
        RBody = GetComponent<Rigidbody2D>();
        newPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Rotation();
    }

    void Rotation()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            transform.Rotate(transform.forward * m_Rotation_Angle);
            newPosition += (transform.up * m_Movement_Speed * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {

            transform.Rotate(transform.forward * -m_Rotation_Angle);
            newPosition += (transform.up * m_Movement_Speed * Time.deltaTime);
        }
       transform.position = Vector3.Lerp(transform.position, newPosition, 0.01f);
    }

    //void Rotation()
    //{
    //    if (Input.GetKeyDown(KeyCode.A))
    //    {
    //        transform.Rotate(transform.forward * m_Rotation_Angle);
    //        transform.Translate(transform.up * m_Movement_Speed * Time.deltaTime);
    //    }
    //    if (Input.GetKeyDown(KeyCode.D))
    //    {
    //        transform.Rotate(transform.forward * -m_Rotation_Angle);
    //        transform.Translate(transform.up * m_Movement_Speed * Time.deltaTime);
    //    }
    //}

}